# OPENER ZHV Tools

This Python toolset can be used for converting ZHV data (German: Daten des Zentralen Haltestellenverzeichnisses) to OSM.
Use case of the converted data is the mapping of accessibility barriers at public transport stops with the [OPENER App](https://gitlab.hrz.tu-chemnitz.de/opener/opener-app).

The tool's software is licensed under the GPL 3.0.


Further information can be found at the German-speaking [project website of OPENER](https://www.bmvi.de/SharedDocs/DE/Artikel/DG/mfund-projekte/opener.html).

The development of this app is sponsored by the Federal Ministry of Transport and Digital Infrastructure under the project ID VB18F1016A 
within the digital innovation program mFUND.

## Dependencies:
- Python 3.8
- pyosmium
- lxml

## Usage:

```shell 
python ZhvConverter.py zHV_aktuell_xml.2020-05-05.xml --regionfilter SACHSEN
```
The output file is in your current working directory and has the name "yyyy-mm-dd-stop_places.osm" (e.g. 2020-05-08-stop_places.osm).

## XML-Scheme of ZHV:

The ZHV classes were generated by [generateDS](https://www.davekuhlman.org/generateDS.html) using a [ZHV XML scheme](data/scheme/zhv_scheme.xsd).

Regeneration of these classes can be done by:
```shell
generateDS -o ZhvClasses.py zhv_scheme.xsd
```