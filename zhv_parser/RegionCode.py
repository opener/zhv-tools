from enum import Enum


# This is representing the first two digits of Germany's "Amtlicher Gemeindeschlüssel":
# https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Glossar/amtlicher-gemeindeschluessel.html
from zhv_parser.ZhvClasses import StopPlace


class RegionCode(Enum):
    ALL = ""
    SCHLESWIG_HOLSTEIN = "01"
    HAMBURG="02"
    NIEDERSACHSEN="03"
    BREMEN="04"
    NORDRHEIN_WESTFALEN="05"
    HESSEN="06"
    RHEINLAND_PFALZ = "07"
    BADEN_WUERTEMBERG = "08"
    BAYERN = "09"
    SAARLAND = "10"
    BERLIN = "11"
    BRANDENBURG = "12"
    MECKLENBURG_VORPOMMERN = "13"
    SACHSEN = "14"
    SACHSEN_ANHALT = "15"
    THUERINGEN = "16"

    def isStopPlaceInRegion(self, stop_place: StopPlace):
        if self == RegionCode.ALL:
            return True
        dhid = stop_place.get_DHID()
        region_code = self.value
        found = str(dhid).find(region_code, 3, 5)
        if found == -1:
            return False
        else:
            return True
