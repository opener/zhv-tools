import argparse
import os
import sys
from datetime import date
from enum import Enum
from typing import Sequence

import osmium
from lxml import etree
from osmium.osm import Location
from osmium.osm.mutable import Relation, Node

from zhv_parser import ZhvClasses
from zhv_parser.RegionCode import RegionCode
from zhv_parser.ZhvClasses import StopPlace, Area, Quay, Name

# ZHV related XML tags, elements
namespace: str = "{http://zhv.xml.wvi/}"
tag_stop_place: str = namespace + "StopPlace"
tag_area: str = namespace + "Area"
tag_quay: str = namespace + "Quay"

event_start: str = "start"
event_end: str = "end"

# OSM related PT tags: https://wiki.openstreetmap.org/wiki/DE:Public_transport
# General tags:
osm_tag_name = ('name', '')  # example: name=TU Campus -> used in relation
# -> for search machines -> used at the stop position
osm_tag_ref = ('ref', '')  # example: ref=TU Campus, Bstg. 3A -> used at the stop platform or stop sign
osm_tag_ref_ifopt = ('ref:IFOPT', '')


# OSM Tags for stop place (relation): also use OSM tag "name=*"
osm_tag_type_pt = ('type', 'public_transport')
osm_tag_stop_place = ('public_transport', 'stop_area')
osm_tag_ref_name = ('ref_name', '')  # example: ref_name=TU Campus, Chemnitz
osm_relation_role_platform = 'platform'
osm_relation_type_node = 'node'
# <member type='node' ref='node_id' role='platform' />
# example:
# <relation id='7795887' timestamp='2019-08-25T21:04:46Z' uid='559480' user='SvenQ' visible='true' version='4' changeset='73726675'>
#     <member type='node' ref='5274416450' role='stop' />
#     <member type='way' ref='545750504' role='platform' />
#     <member type='node' ref='5272770752' role='stop' />
#     <member type='way' ref='545750513' role='platform' />
#     <member type='node' ref='5272770751' role='stop' />
#     <member type='way' ref='545750516' role='platform' />
#     <member type='node' ref='5272770754' role='stop' />
#     <member type='way' ref='545750519' role='platform' />
#     <member type='node' ref='5272770753' role='stop' />
#     <member type='way' ref='545750520' role='platform' />
#     <member type='node' ref='5239435218' role='stop' />
#     <member type='way' ref='545750524' role='platform' />
#     <tag k='fare_zone' v='13' />
#     <tag k='name' v='TU Campus' />
#     <tag k='network' v='Verkehrsverbund Mittelsachsen' />
#     <tag k='operator' v='Chemnitzer Verkehrs-AG' />
#     <tag k='public_transport' v='stop_area' />
#     <tag k='ref_name' v='TU Campus, Chemnitz' />
#     <tag k='type' v='public_transport' />
#   </relation>


# OSM tags for stop positions of buses and trams
osm_tag_stop_position = ('public_transport', 'stop_position')
osm_tag_bus = ('bus', 'yes')
osm_tag_tram = ('tram', 'yes')
osm_tag_tram_stop = ('railway', 'tram_stop')

# for platforms or stop signs (https://wiki.openstreetmap.org/wiki/Tag:public_transport%3Dplatform)
osm_tag_platform_pt = ('public_transport', 'platform')  # for nodes and ways
osm_tag_platform_railway = ('railway', 'platform')  # for nodes and ways that define tram/rail platforms
osm_tag_highway_bus_stop_sign = ('highway', 'bus_stop')  # for nodes that define stop sign positions
#osm_tag_highway_bus_platform = ('highway', 'platform')  # for ways that define bus platform areas or lines
# -> ZHV contains only nodes/points, therefore not necessary
osm_tag_fixme_type_unknown = ('fixme', 'Ist diese Haltestelle für Straßenbahnen, Busse oder etwas ganz anderes?')

# For trams: https://wiki.openstreetmap.org/wiki/Tag:railway%3Dtram_stop


class PlatformType(Enum):
    BUS = osm_tag_highway_bus_stop_sign
    TRAM = osm_tag_platform_railway
    UNKNOWN = osm_tag_fixme_type_unknown

class ZhvConverter:

    def __init__(self):
        self.unused_osm_id = 0

    def parse(self, xml_file, region_code: RegionCode = RegionCode.ALL):
        today = date.today()
        # dd/mm/YY
        d1 = today.strftime("%Y-%m-%d-")
        file_name = d1 + 'stop_places.osm'
        if os.path.exists(file_name):
            os.remove(file_name)

        writer = osmium.SimpleWriter(file_name)
        context = etree.iterparse(xml_file, events=(event_start, event_end),
                                  tag=(tag_stop_place, tag_area, tag_quay))
        for action, elem in context:
            if elem.tag == tag_stop_place and action == event_start:
                stop_place_class = ZhvClasses.StopPlace
                stop_place = stop_place_class.factory()
                stop_place.build(elem)
                if not self.is_stop_place_in_region(region_code, stop_place):
                    continue

                (stop_place_relation, nodes) = self.stop_place_to_osm(stop_place, context)

                if stop_place_relation is not None:
                    for node in nodes:
                        writer.add_node(node)
                    writer.add_relation(stop_place_relation)

        writer.close()

    def is_stop_place_in_region(self, region_code: RegionCode, stop_place: StopPlace):
        return region_code.isStopPlaceInRegion(stop_place)

    def stop_place_to_osm(self, stop_place: StopPlace, context) -> (Relation, Sequence[Node]):
        """Converts stop place to an OSM object"""
        # extract stop place parameters
        # TODO: Ask ZHV: Why are names double encapsulated?
        stop_place_name: str = stop_place.get_Name().get_Name()[0].get_valueOf_()
        # create relation:
        stop_place_relation: Relation = Relation()
        stop_place_relation.members = list()
        stop_place_relation.tags = list(tuple())
        stop_place_relation.tags.append(tuple([osm_tag_ref_name[0], stop_place_name]))
        stop_place_relation.tags.append(osm_tag_type_pt)
        stop_place_relation.tags.append(osm_tag_stop_place)
        stop_place_relation.id = self.get_unused_osm_id()

        list_stop_signs = list()
        for action, elem in context:
            if elem.tag == tag_area and action == event_start:
                area_class = ZhvClasses.Area
                area = area_class.factory()
                area.build(elem)
                list_stop_signs += self.area_to_osm(area, context)

            elif elem.tag == tag_stop_place and action == event_end:
                if len(list_stop_signs) == 0:
                    node_platform = self.create_platform_node(stop_place, PlatformType.UNKNOWN)
                    if node_platform is None:
                        return None, None
                    else:
                        list_stop_signs.append(node_platform)

                for stop_sign in list_stop_signs:
                    stop_place_relation.members.append(tuple([osm_relation_type_node, stop_sign.id, osm_relation_role_platform]))

                return stop_place_relation, list_stop_signs

    def area_to_osm(self, area: Area, context) -> Sequence[Node]:
        list_nodes = list()
        # TODO: extract type of stop sign (Bus, Tram, etc.?) -> unfortunately, there is no standard attribute for this
        # One possible criteria for bus/tram distinguishing: (used by HannIT)
        #             <Description>
        #               <Description>Bus</Description>
        #             </Description>
        # 2nd possible criteria:
        #  <Name>
        #    <Name>Tram</Name> or <Name>Bus</Name>
        #  </Name>

        # name = area.get_Name().get_Name()[0].get_valueOf_()
        # description = area.get_
        stop_sign_type = PlatformType.UNKNOWN

        for action, elem in context:
            if elem.tag == tag_quay and action == event_start:
                quay_class = ZhvClasses.Quay
                quay = quay_class.factory()
                quay.build(elem)
                node = self.quay_to_osm(quay, stop_sign_type, context)
                if node is not None:
                    list_nodes.append(node)

            elif elem.tag == tag_area and action == event_end:
                return list_nodes

    def quay_to_osm(self, quay: Quay, platform_type: PlatformType, context) -> Node:
        """ Extract an OSM node from the given ZHV stop sign structure """
        # extract quay parameters
        # TODO: Ask ZHV: Why are names double encapsulated?
        node_stop_sign: Node = self.create_platform_node(quay, platform_type)

        # return to previous structure hierarchy
        for action, elem in context:
            if elem.tag == tag_quay and action == event_end:
                return node_stop_sign

    def get_unused_osm_id(self):
        self.unused_osm_id -= 1
        return self.unused_osm_id

    def create_platform_node(self, zhv_element, platform_type: PlatformType) -> Node:
        if not isinstance(zhv_element, (StopPlace, Area, Quay)):
            return None

        node_platform: Node = Node()

        try:
            quay_dhid: str = zhv_element.get_DHID()
            if quay_dhid is None:
                raise ValueError('No DHID set!')
            quay_lat: str = zhv_element.get_Location().get_Latitude()
            quay_lon: str = zhv_element.get_Location().get_Longitude()
            node_platform.location = Location(quay_lon, quay_lat)
        except Exception as error:
            print(error)
            return None

        # todo exception handling
        try:
            quay_name: str = zhv_element.get_Name().get_Name()[0].get_valueOf_()
            node_platform.tags.append((osm_tag_ref[0], quay_name))
        except:
            pass

        node_platform.id = self.get_unused_osm_id()

        node_platform.tags = list()
        node_platform.tags.append((osm_tag_ref_ifopt[0], quay_dhid))
        node_platform.tags.append(osm_tag_platform_pt)
        node_platform.tags.append(platform_type.value)
        return node_platform

USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile", help="ZHV input file")
    parser.add_argument("--regionfilter", help="zhv")

    args = parser.parse_args()
    infile_name = args.inputfile
    if args.regionfilter:
        region_code = RegionCode[args.regionfilter]
    else:
        region_code = RegionCode.ALL

    print(f'Start conversion with region filer {region_code}')
    ZhvConverter().parse(infile_name, region_code)


if __name__ == '__main__':
    main()
